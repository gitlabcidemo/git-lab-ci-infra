#! /bin/sh
# Generate resolver.con file before starting nginx
echo resolver $(awk 'BEGIN{ORS=" "} $1=="nameserver" {print $2}' /etc/resolv.conf) " valid=30s;" > /etc/nginx/resolvers.conf

echo "Recreate nginx conf from template, only replace DOMAIN variable"
envsubst \$DOMAIN < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf

echo $ROOT_DOMAIN
cat /etc/nginx/conf.d/default.conf

echo "Now starting nginx"
nginx -g "daemon off;"
