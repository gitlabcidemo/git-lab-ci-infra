# Introduction
Ce code permet d'instancier une infrastructure docker pour faire tourner un GitLab CI avec les outils classiques de développement.
Recommandation de configuration pour Docker : 10Go de RAM et 4 cores mini.

Ce référentiel fonctionne avec une application de démo pour faire tourner GitLab ( https://gitlab.com/gitlabcidemo/git-lab-ci-demo ).
Il faut avoir suivi le tutorial ci-dessous avant de basculer sur le repo de démo.

Les composants :
* gitlab : le serveur GitLab ( http://localhost:480/ )
* gitlabrunner : le runner qui va exécuter les jobs du build
* reverseproxy : Le reverse proxy qui est utilisé pour gérer la redirection vers
    les environnements dynamique ( http://localhost:482/XXX/happystore )
* sonar : le serveur sonar ( http://localhost:9000/ )
* sonardb : la base de données de sonar
* docker-registry-web : Interface web du registre docker ( http://localhost:481 )
* docker-registry : le registre docker, dans cette démo on n'utilise pas celui qui est intégré dans gitlab
* weave-scope : Monitoring docker ( http://localhost:4040/ )

# Mise en place de l'Infra

## Installation du GitLab

### Lancer toute l'infra
```
cd docker
ducker-compose up -d --build
```

Verifier les logs du container gitlab, s'il a du mal à démarrer, c'est un bug connu du container, dans ce cas :
1. Lancer la commande `docker exec -it gitlab update-permissions`
2. redémarrer le container `docker restart gitlab`.

Vérifier que vous arrivez bien a vous connecter aux applications (cf liste de lien ci dessus).

### Créer le compte admin sur GitLab
http://localhost:480/
Créer un compte admin, par exemple : root / gitlab01
Ajouter votre clé SSH au compte admin, si vous souhaitez vous connecter depuis votre laptop (host).

### Créer le repo git : _git-lab-ci-demo_
* Créer un jeu de clé qui va être utilisé pour que le gitlab runner puisse merger des branches
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

* Ajouter la clé public au compte utilisé par l'agent gitlab runner (qui est utiliser pour pousser sur la branche UAT).
_GitLabRunnerForGit_
http://localhost:480/root/git-lab-ci-demo/settings/repository
Partie deploy key, cocher la case authorisé à faire du _push_.

* Créer une variable _CI_GIT_PRIVATE_KEY_ non protégée avec la clée privée
http://localhost:480/root/git-lab-ci-demo/settings/ci_cd

## Installation du gitlab runner
### Enregistrement du runner en tant que global runner
Il faut obtenir le _Registration Token_ GitLab pour créer le runner :
http://localhost:480/admin/runners

```
docker exec -it gitlabrunner bash
export RegistrationToken=xjE18vErB3pVxDbssE7s
gitlab-runner register -n -r $RegistrationToken --executor docker --docker-image maven -u http://gitlab \
 --docker-network-mode docker_gitlabNet --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```

### Augmentation du nombre de jobs en parallèle
Avec _concurrent_ et _limit_ https://gitlab.com/gitlab-org/gitlab-runner/blob/master/docs/configuration/advanced-configuration.md
```
docker exec -it gitlabrunner bash
# Mettre 4 ou plus sur concurrent
# Mettre "privileged = true" pour utiliser docker:dind
# QQQ Mettre "Shared = false"
vi /etc/gitlab-runner/config.toml
exit
docker restart gitlabrunner
```

## Activation du runner pour notre projet
Aller sur le runner `http://localhost:480/admin/runners/1` et activer le pour votre projet.


## Registre Docker
### Instancier la conf du registre
Il est déjà configuré en suivant la doc : https://hub.docker.com/r/hyper/docker-registry-web/, partie _With authentication enabled_.
Il faut cependant générer des certificats pour faire une relation de confiance entre le container registry et son container d'interface web.
```
openssl req -new -newkey rsa:4096 -days 365 -subj "/CN=localhost" \
        -nodes -x509 -keyout docker/registry/conf/auth.key -out docker/registry/conf/auth.cert
```

### Se loguer dans la console http://localhost:481/
Avec Admin / Admin
#### Créer un utilisateur _dockerci_ avec un mot de passe
#### Donner lui le role `write_all` (nécessaire pour pousser dans le repo, on peut faire plus fin comme droit)

### Renseigner la configuration du compte `dockerci` dans votre projet _git-lab-ci-demo_
#### Dans une variable _CI_BUILD_TOKEN_ pour pouvoir pousser les images que vous allez fabriquer lors du packaging
Dans http://localhost:480/root/git-lab-ci-demo/settings/ci_cd , renseigner la variable secrête _CI_BUILD_TOKEN_ avec le mot de passe du compte _dockerci_ que vous venez de créer.

#### Dans une variable _DOCKER_AUTH_CONFIG_ pour pouvoir utiliser des images de votre regitre pour faire votre build.
Plus d'information ici : https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#define-an-image-from-a-private-docker-registry

Définir une variable secrête _DOCKER_AUTH_CONFIG_ dans le projet.
Depuis la page d'admin : http://localhost:480/root/git-lab-ci-demo/settings/ci_cd
```
{
    "auths": {
    "<registry>": {
            "auth": "<authvalue>",
        }
    }
}
```
Avec :
* _<registry>_ = dockerregistry:5000
* _<authvalue>_ = $(echo -n "my_username:my_password" | base64)

Exemple pour _dockerregistry:5000_ avec le user _dockerci_ et le mot de passe _GhpbGlwcGUiLCJuYmYiOjE1MTExMzIyMTcsImV4cCI6_ :
```
{
"auths": {
"dockerregistry:5000": {
        "auth": "ZG9ja2VyY2k6R2hwYkdsd2NHVWlMQ0p1WW1ZaU9qRTFNVEV4TXpJeU1UY3NJbVY0Y0NJNg=="
    }
}
}
```

#### Pousser les images dans le registre
Pousser ensuite vos images techniques dans votre repository docker pour les mettre à disposition des builds, lancer la commande suivante :
```
docker-compose -f docker-compose-tool.yml build
# Depuis votre poste le registre est 'localhost:5000', depuis les containers docker ce sera 'dockerregistry:5000'.
docker login -u dockerci localhost:5000
docker-compose -f docker-compose-tool.yml push
```



# La suite...
Dans le readme du repo GitLabCIDemo...


